import { fileURLToPath } from 'url';
import {dirname, join} from "path"
/** @type {import('next').NextConfig} */
const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const nextConfig = {
    images:{
        loader: "custom",
        loaderFile: "./src/utils/loader.js",
    },
    sassOptions:{
        includePaths: [join(__dirname, 'src/sass')],
        prependData: `@import "main"`
    }
};

export default nextConfig;
