"use server"
import { z } from 'zod'
import { GraphQLClientSingleton } from '@/graphql/GraphQLClientSingleton'
import {createUserMutation} from "@/graphql/mutations/createUserMutation";
import {createAccessToken} from "@/utils/auth/createAccessToken";
import {redirect} from "next/navigation";
import {revalidatePath} from "next/cache";
import {ClientType, State} from "@/lib/definitions";


const BaseSchema = z.object({
    email: z.string({
        invalid_type_error: 'Por favor introduzca una dirección de correo electrónico válida',
    }).email({ message: "Invalid email" }),
    password: z.string({
        invalid_type_error: 'Por favor introduce una contraseña válida',
    }).min(8, { message: "Debe tener al menos 8 caracteres de longitud" }).regex(/^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)[A-Za-z\d@$!%*?&]{8,}$/, { message: "La contraseña debe contener al menos una letra mayúscula, una letra minúscula y un número, y al menos 8 o más caracteres, sin espacios en blanco, caracteres especiales permitidos."}),
})
const FormSchema = z.object({
    firstName: z.string({
        invalid_type_error: 'Por favor, ingrese un nombre válido',
    }).min(4, { message: "El nombre debe tener al menos 4 caracteres" }).regex(/^[a-zA-Z\s]*$/, { message: "El nombre debe contener solo letras." }),
    lastName: z.string({
        invalid_type_error: 'Por favor ingrese un apellido válido',
    }).min(4, { message: "El apellido debe tener al menos 4 caracteres." }).regex(/^[a-zA-Z\s]*$/, { message: "El apellido debe contener solo letras." }),
    phone: z.string({
        invalid_type_error: 'Por favor ingrese un número de teléfono válido',
    }).regex(/^[0-9]*$/, { message: "El número de teléfono debe contener solo números." }).length(8, { message: "El número de teléfono debe tener 8 caracteres" }),
    rePassword: z.string({
        invalid_type_error: 'Por favor introduce una contraseña válida',
    }),
})

const CreateClient = FormSchema.merge(BaseSchema).required({
    firstName: true,
    lastName: true,
    email: true,
    phone: true,
    password: true,
    rePassword: true
})

export async function handleCreateUser(prevState:State, formData: FormData){
    const validateFields = CreateClient.safeParse({
        firstName: formData.get('firstName'),
        lastName: formData.get('lastName'),
        email: formData.get('email'),
        phone: formData.get('phone'),
        password: formData.get('password'),
        rePassword: formData.get('re_password')
    })
    if (!validateFields.success){
        return {
            errors: validateFields.error.flatten().fieldErrors,
            message: 'Missing Fields. Failed to create invoice.',
        }
    }
    if (validateFields.data.password !== validateFields.data.rePassword){
        return {
            errors: {password: ['La contraseña no coincide']},
            message: 'La contraseña no coincide'
        }
    }
    const { firstName, lastName, email, phone, password } = validateFields.data
    const client = GraphQLClientSingleton.getInstance().getClient()
    const variables = {
        input: {
            firstName: firstName,
            lastName: lastName,
            email: email,
            phone: `+591${phone}`,
            password: password
        }
    }
    try {
        const { customerCreate } = await client.request(createUserMutation, variables) as ClientType
        const { customerUserErrors, customer } = customerCreate
        if (customer?.firstName) {
            await createAccessToken(email as string, password as string)
            console.log('customer', customer)
        }
    }catch (e: any){
        return {error: e instanceof z.ZodError ? e.issues : [{ path: ['unknown'], message: e.message }]}
    }
    revalidatePath('/store');
    redirect('/store');

}

export async function handleSignInUser(prevState:State, formData: FormData){
    const validateFields = BaseSchema.safeParse({
        email: formData.get('email'),
        password: formData.get('password')
    })

    if (!validateFields.success){
        return {
            errors: validateFields.error.flatten().fieldErrors,
            message: 'Missing Fields. Failed to create invoice.',
        }
    }
    const { email, password } = validateFields.data

    try {
        const accessToken = await createAccessToken(email as string, password as string)
        if (accessToken){
            console.log('accessToken', accessToken)
        }
    }catch (e: any){
        return {error: e instanceof z.ZodError ? e.issues : [{ path: ['unknown'], message: e.message }]}
    }
    revalidatePath('/store');
    redirect('/store');
}