"use client";
import {BgSvg} from "@/components/ui/BgSvg";

interface ErrorProps {
    error: Error;
    reset: () => void;
}

export default function Error({ error, reset }: ErrorProps) {

    return (
        <main className="relative overflow-hidden grid min-h-full place-items-center px-6 py-24 sm:py-32 lg:px-8">
            <BgSvg>
                <div className="text-center">
                    <p className="text-2xl font-semibold text-indigo-600">404</p>
                    <h1 className="mt-4 text-3xl font-bold tracking-tight text-gray-900 dark:text-white sm:text-5xl">Page not found</h1>
                    <p className="mt-6 text-base leading-7 text-gray-600">Lo sentimos, ocurrio un error.</p>
                    <div className="mt-10 flex items-center justify-center gap-x-6">
                        <button onClick={reset} className="border border-blue-700 px-3 py-2 rounded-xl text-sm font-semibold text-gray-900 dark:text-indigo-500"
                                style={{
                                    background: 'rgba(0, 0, 0, 0.5)',
                                }}
                        >
                            Intentar de nuevo <span aria-hidden="true">&rarr;</span>
                        </button>
                    </div>
                </div>
            </BgSvg>
        </main>
    )
}