import {Hero} from "@/components/home/Hero";
import {Description} from "@/components/home/Description";
import {Services} from "@/components/home/services/Services";

export default function HomeLayout({children}:{children: React.ReactNode}){
    return(
        <>
            <Hero />
            <Description />
            <Services/>
            {children}
        </>
    )

}