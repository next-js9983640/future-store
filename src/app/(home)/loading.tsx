import {Title} from "@/components/ui/Title/Title";

export default function Loading() {
    return(
        <div className='flex flex-col justify-center items-center h-screen'>
            <div className="animate-spin rounded-full h-48 w-48 border-t-8 border-b-8 border-[#0022FFFF]"></div>
            <Title text='Loading...' classNames='text-4xl'/>
        </div>
    )
}