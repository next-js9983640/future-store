
import {MainProducts} from "@/components/home/MainProducts/MainProducts";
export default async function Home() {

  return (
    <main className='py-5'>
        <MainProducts />
    </main>
  );
}
