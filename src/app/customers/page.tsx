import { getDataConfig } from '@/config/getDataConfig'
import {dataFeching} from "@/services/api/DataFeching";
export default async function CustomersPage(){
    const { shopifyGetCustomersHeaders } = getDataConfig
    const customers = await dataFeching(shopifyGetCustomersHeaders)

    return (
        <div>
            <h1>Customers</h1>
        </div>
    )
}