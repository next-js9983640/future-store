import {Gallery} from "@/components/home/Gallery";
import {Suspense} from "react";
import {ImageSkeletonContainer} from "@/components/ui/Skeleton/Skeleton";
import {Pagination} from "@/components/ui/Gallery/Pagination";
import { createClientComponentClient } from '@supabase/auth-helpers-nextjs'
import {BgSvg} from "@/components/ui/BgSvg";
export default async function GalleryPage({searchParams}:{searchParams?: {query?: string, page?: number}}){
    const supabase = await createClientComponentClient()
    const {count} = await supabase.from('images').select('id', {count: 'exact'})
    const currentPage = Number(searchParams?.page) || 1
    const query = searchParams?.query || ''
    const ItemsPerPage = 10
    const totalPages = Math.ceil(Number(count)/ ItemsPerPage)

    return (
        <div className='w-[98%] mx-auto mt-24 my-10 relative'>
            <BgSvg>
                <div className="m-5 flex w-auto mx-auto justify-center">
                    <Pagination totalPages={totalPages}/>
                </div>
                <Suspense fallback={<ImageSkeletonContainer/>}>
                    <Gallery currentPage={currentPage}/>
                </Suspense>
            </BgSvg>
        </div>
    )
}