import Link from 'next/link'
import {NotFoundUi} from "@/components/ui/NotFound";

export default function NotFound() {
    return (
        <div className='relative flex justify-center items-center flex-col'>
            <NotFoundUi />
            <Link className='absolute bg-indigo-950 py-3 px-3 rounded-xl' href="/">Return Home</Link>
        </div>
    )
}