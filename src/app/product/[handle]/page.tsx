import {ProductView} from "@/components/store/product/ProductView";
import {dataFeching} from "@/services/api/DataFeching";
import {getDataConfig} from "@/config/getDataConfig";
import {redirect} from "next/navigation";

interface productProps {
    params: {
        handle: string
    },
    searchParams: {
        id: string
    }
}

export default async function ProductPage(props: productProps){
    const {searchParams:{id}} = props
    const { shopifyGetProductsHeaders } = getDataConfig
    const products = await dataFeching(shopifyGetProductsHeaders, id)
    const product = products[0]
    if (!id) redirect('/store')
    return(
        <ProductView product={product}/>
    )
}