import {Title} from "@/components/ui/Title/Title";

export default function Loading() {
    return (
        <div className='flex flex-col justify-center items-center h-[60vh] my-10 w-full'
             style={{
                 background: 'rgba(0,0,0,0.7)'
             }}
        >
            <div className="absolute rounded-full animate-spin  h-52 w-52 border-t-4 border-[#0022FFFF]"></div>

            <Title text='Cargando...' classNames='text-4xl'/>
        </div>
    )
}