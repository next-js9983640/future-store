import {ProductWrapper} from "@/components/store";
import {getDataConfig} from "@/config/getDataConfig";
import {ShopifyCollectionsType, ShopifyProductType} from "@/lib/definitions";
import {dataFeching} from "@/services/api/DataFeching";

interface categoryProps {
    params: {
        categories: string[],
        searchParams?: string
    }
}

export default async function Category(props : categoryProps){
    const {params:{categories}} = props
    const { shopifyGetProductsHeaders, shopifyGetCollectionsHeaders, shopifyGetProductsByCollection } = getDataConfig
    let products: ShopifyProductType[] = []


    const collections: ShopifyCollectionsType[] = await dataFeching(shopifyGetCollectionsHeaders)

    //const productsByCollection: ShopifyProductType[] = await dataFeching(shopifyGetProductsByCollection(categories[0]? categories[0] : ''))
    if (categories?.length > 0){
        const getCollection = collections.find((item) => item.handle === categories[0])?.id
        products = await dataFeching(shopifyGetProductsByCollection(String(getCollection)))
        const productPrice:ShopifyProductType[] = await dataFeching(shopifyGetProductsHeaders)
        productPrice.forEach((product) => {
            const price = product.variants[0].price
            products.forEach((item) => {
                if (item.id === product.id){
                    item.price = price
                }
            })
        })
        console.log(products)
    }else {
        products = await dataFeching(shopifyGetProductsHeaders)
    }
    return(
        <ProductWrapper products={products} />
    )
}