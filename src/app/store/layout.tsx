import {ReactNode} from 'react'
import {BgSvg} from "@/components/ui/BgSvg";
import {Collections} from "@/components/store";

export default function Layout({children}: {children: ReactNode}){

    return(
        <section className='relative w-[95%] mx-auto pt-10'>
            <BgSvg>
                <Collections/>
                {children}
            </BgSvg>
        </section>
    )
}