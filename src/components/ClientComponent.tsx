"use client"

export function  ClientComponent({action}:{action: ()=>void}) {
  return (
      <form action={action} className='flex justify-center'>
          <button className='btn btn-primary'>Load More</button>
      </form>
  );
}