"use client"
import {useState} from "react";
import {BgSvg} from "@/components/ui/BgSvg";
import {handleSignInUser} from "@/actions";
import {Input} from "@/components/ui/Input";
import Link from "next/link";
import {Logo} from "@/components/ui/icons/Logo";
import {Title} from "@/components/ui/Title/Title";
import { useFormState } from 'react-dom'

export function SignInUi(){
    const initalState = {message:null, errors: {}}
    // @ts-ignore
    const [state, dispatch]= useFormState(handleSignInUser, initalState)
    return (
        <>
            <div className="relative px-6 py-12 lg:px-8"

            >
                <BgSvg>
                    <div className="rounded-md mt-10 sm:mx-auto sm:w-full sm:max-w-sm border p-4 grid grid-cols-1"
                         style={{
                             background: 'rgba(0, 0, 0, 0.5)'
                         }}
                    >
                        <div className="sm:mx-auto sm:w-full sm:max-w-sm mb-5">

                            <div className='flex justify-center items-center'>
                                <Logo/>
                            </div>
                            <Title text={'Inicio de sesión'}
                                   classNames={'text-center text-2xl font-bold leading-9 tracking-tight'}/>
                        </div>
                        <form autoComplete="on" className="space-y-6" action={dispatch}>
                            <Input type={'email'} label={'Correo electrónico'} placeholder={'alguien@gmail.com'}
                                   name={'email'}>
                                <div id='customer-error' aria-live='polite' aria-atomic='true'>
                                    {state?.errors?.email &&
                                        state.errors?.email.map((error) => (
                                            <p key={error} className='mt-2 text-sm text-red-500'>
                                                {error}
                                            </p>
                                        ))}
                                </div>
                            </Input>
                            <Input type={'password'} label={'Contraseña'} placeholder={'*********'} name={'password'}>
                                <div id='customer-error' aria-live='polite' aria-atomic='true'>
                                    {state?.errors?.password &&
                                        state.errors?.password.map((error) => (
                                            <p key={error} className='mt-2 text-sm text-red-500'>
                                                {error}
                                            </p>
                                        ))}
                                </div>
                            </Input>

                            <div id='customer-error' aria-live='polite' aria-atomic='true'>
                                {state.message && (
                                    <p className='mt-2 text-sm text-green-500'>
                                        {state.message}
                                    </p>
                                )}
                            </div>

                            <div className="text-sm">
                                <a href="#" className="font-semibold text-indigo-600 hover:text-indigo-500">
                                    ¿Has olvidado tu contraseña?
                                </a>
                            </div>
                            <div>
                                <button
                                    type="submit"
                                    className="flex w-full justify-center rounded-md bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                                >
                                    Iniciar sesión
                                </button>
                            </div>
                        </form>

                        <div className="grid grid-cols-1 gap-2">
                            <p className="mt-10 text-center text-sm text-gray-500">
                                ¿No eres un miembro?{' '}
                            </p>
                            <div>
                                <Link
                                    href="/signup"
                                    className="flex w-full justify-center rounded-md bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                                >

                                    Crear cuenta
                                </Link>
                            </div>
                        </div>

                    </div>
                </BgSvg>
            </div>
        </>
    )
}