"use client"
import {Logo} from "@/components/ui/icons/Logo";
import {BgSvg} from "@/components/ui/BgSvg";
import { useState } from 'react'
import {handleCreateUser} from "@/actions";
import {Input} from "@/components/ui/Input";
import Link from "next/link";
import {Title} from "@/components/ui/Title/Title";
import {useFormState} from 'react-dom'
export function SingUpUi() {
    const initialState = { message: null, errors: {} };
    // @ts-ignore
    const [state, dispatch] = useFormState(handleCreateUser, initialState);
    return (
        <>
            <div className="relative px-6 py-12 lg:px-8"

            >
                <BgSvg>
                    <div className="rounded-md mt-10 sm:mx-auto sm:w-full sm:max-w-sm border p-4 grid grid-cols-1"
                         style={{
                             background: 'rgba(0, 0, 0, 0.5)'
                         }}
                    >
                        <div className="sm:mx-auto sm:w-full sm:max-w-sm mb-5">

                            <div className='flex justify-center items-center'>
                                <Logo/>
                            </div>
                            <Title text={'Crea una cuenta nueva'}
                                   classNames={'text-center text-2xl font-bold leading-9 tracking-tight'}/>
                        </div>
                        <form autoComplete="on" className="space-y-6" action={dispatch}>
                            <div className="flex gap-5">
                                <Input type={'text'} label={'Nombre'} placeholder={'alguien'} name={'firstName'}>
                                    <div id='customer-error' aria-live='polite' aria-atomic='true'>
                                        {state?.errors?.firstName &&
                                            state.errors?.firstName.map((error) => (
                                                <p key={error} className='mt-2 text-sm text-red-500'>
                                                    {error}
                                                </p>
                                            ))}
                                    </div>
                                </Input>
                                <Input type={'text'} label={'Apellido'} placeholder={'alguien'} name={'lastName'}>
                                    <div id='customer-error' aria-live='polite' aria-atomic='true'>
                                        {state?.errors?.lastName &&
                                            state.errors?.lastName.map((error) => (
                                                <p key={error} className='mt-2 text-sm text-red-500'>
                                                    {error}
                                                </p>
                                            ))}
                                    </div>
                                </Input>
                            </div>
                            <Input type={'email'} label={'Correo electrónico'} placeholder={'alguien@gmail.com'}
                                   name={'email'}>
                                <div id='customer-error' aria-live='polite' aria-atomic='true'>
                                    {state?.errors?.email &&
                                        state.errors?.email.map((error) => (
                                            <p key={error} className='mt-2 text-sm text-red-500'>
                                                {error}
                                            </p>
                                        ))}
                                </div>
                            </Input>
                            <Input type={'text'} label={'Número de teléfono'} placeholder={'75956535'}
                                   name={'phone'}>
                                <div id='customer-error' aria-live='polite' aria-atomic='true'>
                                    {state?.errors?.phone &&
                                        state.errors?.phone.map((error) => (
                                            <p key={error} className='mt-2 text-sm text-red-500'>
                                                {error}
                                            </p>
                                        ))}
                                </div>
                            </Input>
                            <Input type={'password'} label={'Contraseña'} placeholder={'*********'} name={'password'}>
                                <div id='customer-error' aria-live='polite' aria-atomic='true'>
                                    {state?.errors?.password &&
                                        state.errors?.password.map((error) => (
                                            <p key={error} className='mt-2 text-sm text-red-500'>
                                                {error}
                                            </p>
                                        ))}
                                </div>
                            </Input>
                            <Input type={'password'} label={'Repita la contraseña'} placeholder={'*********'}
                                   name={'re_password'}>
                                <div id='customer-error' aria-live='polite' aria-atomic='true'>
                                    {state?.errors?.rePassword &&
                                        state.errors?.rePassword.map((error) => (
                                            <p key={error} className='mt-2 text-sm text-red-500'>
                                                {error}
                                            </p>
                                        ))}
                                </div>
                            </Input>


                            <div>
                                <button
                                    type="submit"
                                    className="flex w-full justify-center rounded-md bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                                >
                                    Crear cuenta
                                </button>
                            </div>
                        </form>

                        <div className="grid grid-cols-1 gap-2">
                            <p className="mt-10 text-center text-sm text-gray-500">
                                ¿Ya es un miembro?{' '}
                            </p>
                            <div>
                                <Link
                                    href="/signin"
                                    className="flex w-full justify-center rounded-md bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                                >
                                    Iniciar sesión
                                </Link>
                            </div>
                        </div>

                    </div>
                </BgSvg>
            </div>
        </>
    )
}
