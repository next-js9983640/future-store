import {Card} from "@/components/ui/Card";
import { v4 } from 'uuid'
import {ImageType} from "@/lib/definitions";
import {createServerComponentClient} from "@supabase/auth-helpers-nextjs";
import {cookies} from "next/headers";

const ITEMS_PER_PAGE = 10;
export async function Gallery({currentPage}:{ currentPage: number}){
    const supabase = await createServerComponentClient({cookies})
    const offset = (currentPage - 1) * ITEMS_PER_PAGE

    const {data} = await supabase.from('images').select('*').order('created_at', {ascending: false}).limit(ITEMS_PER_PAGE).range(offset, offset + ITEMS_PER_PAGE - 1)
    const images: ImageType[] = data as ImageType[]


    return (
        <div className="grid grid-cols-1 gap-7 my-10">
            <div className="w-[95%] mx-auto columns-1 sm:columns-2 md:columns-3 xl:columns-4 2xl:columns-5 gap-4">
                {images.map((image: ImageType) => (
                    <Card key={v4()} image={image}/>
                ))}
            </div>
        </div>
    )
}