import styles from './Hero.module.sass'
import {Title} from "@/components/ui/Title/Title";
import {BgSvg} from "@/components/ui/BgSvg";
export function Hero() {

  return (
    <section className={`${styles.Hero} relative`}>
        <BgSvg>
            <Title text='Future world' classNames='text-7xl md:text-9xl' />
            <h2>¡Empoderando tu mañana, hoy!</h2>
        </BgSvg>
    </section>
  );
}