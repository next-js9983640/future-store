import {Title} from "@/components/ui/Title/Title";
import {ShopifyProductType} from "@/lib/definitions";
import {BgSvg} from "@/components/ui/BgSvg";
import {dataFeching} from "@/services/api/DataFeching";
import {getDataConfig} from "@/config/getDataConfig";
import Marquee  from 'react-fast-marquee'
import {MarqueeProvider} from "@/components/ui/Marquee/Marquee";

export async function MainProducts() {
    const { shopifyGetMainProductsHeaders } = getDataConfig
    const products: ShopifyProductType[] = await dataFeching(shopifyGetMainProductsHeaders)
  return (
      <section className='relative'>
          <BgSvg>
              <Title classNames='text-5xl md:text-7xl my-10' text={'✨ ¡Nuevos productos lanzados!'}/>
              <MarqueeProvider className="my-10" >
                  <div className="w-full flex flex-1 gap-5 justify-evenly">
                      {products?.map((product: ShopifyProductType) => (
                          <div
                              key={product.id}
                              className="overflow-hidden after:content group mb-5 block md:w-[500px] w-[500px] rounded-md"
                          >
                              <img
                                  src={product.image.src}
                                  alt={product.body_html}
                                  className="transform brightness-90 transition will-change-auto group-hover:brightness-110"
                                  width={product.image.width}
                                  height={product.image.height}
                              />
                          </div>
                      ))}
                  </div>
              </MarqueeProvider>
          </BgSvg>

      </section>
);
}