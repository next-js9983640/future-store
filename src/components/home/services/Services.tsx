import {Title} from "@/components/ui/Title/Title";
import {Preview, ShopifyPreview} from "@/components/ui/Gallery/Preview";

export function Services() {

    return (
        <>
            <Title text='Servicios' classNames='text-5xl md:text-7xl my-10' />
            <div className='grid grid-cols-1 md:grid-cols-3 w-[90%] mx-auto gap-4 py-5'>
                <div className="overflow-hidden transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 duration-300 border relative p-4 rounded-lg flex justify-center items-center text-white text-center h-[300px]"
                >
                    <div className="after:md:text-nowrap after:text-wrap hover:after:rounded-2xl hover:after:content-['Gallery'] after:flex after:justify-center after:items-center after:text-7xl after:font-bold after:text-center
                    after:absolute after:left-0 after:right-0 after:top-0 after:bottom-0 hover:after:backdrop-blur-sm after:bg-clip-text after:text-transparent
                    after:bg-gradient-to-r after:from-blue-600 after:to-pink-600"/>
                    <div className="absolute -z-50"><Preview /></div>
                </div>
                <div
                    className="overflow-hidden transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 duration-300 border relative p-4 rounded-lg flex justify-center items-center text-white text-center h-[300px]"
                >
                    <div className="after:md:text-nowrap after:text-wrap hover:after:rounded-2xl hover:after:content-['Store'] after:flex after:justify-center after:items-center after:text-7xl after:font-bold after:text-center
                    after:absolute after:left-0 after:right-0 after:top-0 after:bottom-0 hover:after:backdrop-blur-sm after:bg-clip-text after:text-transparent
                    after:bg-gradient-to-r after:from-blue-600 after:to-pink-600"/>
                    <div className="absolute -z-50"><ShopifyPreview/></div>
                </div>
                <div
                    className="overflow-hidden transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 duration-300 border relative p-4 rounded-lg flex justify-center items-center text-white text-center h-[300px]"
                >
                    <div className="after:md:text-nowrap after:text-wrap hover:after:rounded-2xl hover:after:content-['AI_ChatBot'] after:flex after:justify-center after:items-center after:text-7xl after:font-bold after:text-center
                    after:absolute after:left-0 after:right-0 after:top-0 after:bottom-0 hover:after:backdrop-blur-sm after:bg-clip-text after:text-transparent
                    after:bg-gradient-to-r after:from-blue-600 after:to-pink-600"/>
                    <div className="absolute -z-50"><Preview/></div>
                </div>
            </div>
        </>
    )
}