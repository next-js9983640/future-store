export function Footer(){
    return (
        <footer className="h-[20vh] w-full text-3xl text-center border-t-2 font-bold flex justify-center items-center">
            <p>Future World 2024</p>
        </footer>
    )
}