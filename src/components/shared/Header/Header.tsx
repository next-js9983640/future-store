import { NavBar } from "@/components/ui/NavBar/NavBar";
import {Navigation} from "@/lib/data";

export function Header() {
  return (
    <header >
        <NavBar navigation={Navigation} />
    </header>
  );
}