import {getDataConfig} from "@/config/getDataConfig";
import {ShopifyCollectionsType} from "@/lib/definitions";
import {dataFeching} from "@/services/api/DataFeching";
import Link from "next/link";
import {usePathname} from "next/navigation";

export async function Collections(){
    const { shopifyGetCollectionsHeaders } = getDataConfig
    const collections: ShopifyCollectionsType[] = await dataFeching(shopifyGetCollectionsHeaders)
    return (
        <div className='flex md:flex-row flex-col justify-evenly items-center gap-2'>
            <Link href={`/store`} className="text-center px-3 py-1 group relative inline-block overflow-hidden rounded border-gray-100 text-lg font-bold bg-clip-text
                      text-transparent bg-gradient-to-r from-blue-600 to-pink-600">
                <span className="ease absolute left-0 top-0 h-0 w-0 border-t-2 border-blue-600 transition-all duration-200 group-hover:w-full"></span>
                <span className="ease absolute right-0 top-0 h-0 w-0 border-r-2 border-blue-600 transition-all duration-200 group-hover:h-full"></span>
                <span className="ease absolute bottom-0 right-0 h-0 w-0 border-b-2 border-pink-600 transition-all duration-200 group-hover:w-full"></span>
                <span className="ease absolute bottom-0 left-0 h-0 w-0 border-l-2 border-pink-600 transition-all duration-200 group-hover:h-full"></span>
                Todos
            </Link>
            {collections?.map((collection: ShopifyCollectionsType) =>(
                <Link href={`/store/${collection.handle}`}
                      className="text-center px-3 py-1 group relative inline-block overflow-hidden rounded border-gray-100 text-lg font-bold bg-clip-text
                      text-transparent bg-gradient-to-r from-blue-600 to-pink-600" key={collection.id}>
                    <span className="ease absolute left-0 top-0 h-0 w-0 border-t-2 border-blue-600 transition-all duration-200 group-hover:w-full"></span>
                    <span className="ease absolute right-0 top-0 h-0 w-0 border-r-2 border-blue-600 transition-all duration-200 group-hover:h-full"></span>
                    <span className="ease absolute bottom-0 right-0 h-0 w-0 border-b-2 border-pink-600 transition-all duration-200 group-hover:w-full"></span>
                    <span className="ease absolute bottom-0 left-0 h-0 w-0 border-l-2 border-pink-600 transition-all duration-200 group-hover:h-full"></span>
                    {collection.title}
                </Link>
            ))}
        </div>
    )
}