"use client";
import Link from "next/link";

export function CardWrapper({children}: {children: React.ReactNode}) {
    return (
        <Link href={'/'} className="relative w-full">
            {children}
        </Link>
    )
}