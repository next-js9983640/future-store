import {ShopifyProductType} from "@/lib/definitions";
import {Title} from "@/components/ui/Title/Title";
import Link from "next/link";

export async function ProductCard({product}: {product: ShopifyProductType}) {
    const { variants, id, handle, title, tags, image, body_html, status, price  } = product
    return (
        <Link href={`/product/${handle}?id=${id}`} className="relative w-full">
            <div
                key={id}
                className={`overflow-hidden relative duration-300 border rounded-lg flex flex-col text-white text-center h-auto ${status === 'active' ? 'border-[#0022FFFF]' : 'border-[#FF0000FF]'}`}
                style={{
                    boxShadow: `0 0 10px ${status === 'active' ? 'rgb(0,34,255)' : 'rgb(255,0,0)'}`
                }}
            >
                <img
                    src={image.src}
                    alt={body_html}
                    className="transform brightness-90 transition will-change-auto group-hover:brightness-110"
                    style={{transform: "translate3d(0, 0, 0)"}}
                    width={image.width}
                    height={image.height}
                />
                <div
                    className=' top-0 left-0 right-0 bottom-0 bg-indigo-900/50 h-full divide-y-2 divide-indigo-900/50 flex flex-col justify-between'
                    style={{
                        background: 'rgba(0, 0, 0, 0)'
                    }}
                >
                    <div className='p-4' style={{
                        background: 'rgba(0, 0, 0, 0.8)'
                    }}>
                        <Title classNames='text-2xl' text={title}/>
                    </div>
                    <div
                        className={`flex flex-col justify-between items-center gap-2 p-4`}
                        style={{
                            background: 'rgba(0, 0, 0, 0.8)'
                        }}
                    >
                        <div className='flex flex-wrap justify-center items-center gap-1 p-4'

                        >
                            {tags.split(' ').map((tag, index) => (
                                <span
                                    key={index}
                                    className="inline-flex items-center rounded-md border border-indigo-900 px-2 py-1 text-xs font-medium text-indigo-500 ring-1 ring-inset ring-indigo-700/10"
                                >
                                        {tag}
                                    </span>
                            ))}

                        </div>
                        <span className="w-fit block bg-red-600 text-white p-1 rounded-md">
                            Bs {!price && variants ? (
                            variants[0].price) : (price)} BOB
                    </span>
                    </div>
                </div>
            </div>
        </Link>
    )
}