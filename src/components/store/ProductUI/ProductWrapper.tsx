import {ShopifyProductType} from "@/lib/definitions";
import {ProductCard} from "@/components/store/ProductUI/ProductCard";

export function ProductWrapper({products}:{products: ShopifyProductType[]}) {
    return (
        <div className='grid grid-cols-1 md:grid-cols-4 gap-4 py-5 place-content-center'>
            {products?.map((product: ShopifyProductType) => (
                <ProductCard product={product} key={product.id}/>
            ))}
        </div>
    )
}