import {ShopifyProductType} from "@/lib/definitions";
import {ProductViewItemsOrder} from "@/components/store/product/ProductViewItemsOrder";
import {BgSvg} from "@/components/ui/BgSvg";
import {Title} from "@/components/ui/Title/Title";
import {removeHtmlTags} from "@/utils/removeHtmlTag";

export function ProductView({product}:{product: ShopifyProductType}){
    const { id, image, handle, title, price, tags, status, variants, body_html, quantity } = product
    return(
        <main className="md:max-w-[70%] max-w-[90%] mx-auto my-10 grid grid-cols-1 md:grid-cols-2 gap-5 mt-20 overflow-hidden rounded-xl border-2
        border-indigo-900 text-white relative"
            style={{
                boxShadow: '0 0 10px #1e1b4b inset, 0 0 10px #1e1b4b, 0 0 21px #1e1b4b inset, 0 0 21px #1e1b4b, 0 0 31px #1e1b4b inset, 0 0 31px #1e1b4b, 0 0 42px #0c4a6e inset, 0 0 42px #0c4a6e',
                background: 'rgba(0, 0, 0, 0.5)',
            }}
        >
            <BgSvg >
                <section className={'justify-self-end'}>
                    <img src={image.src} alt={product.title} className="w-full h-full object-cover "/>
                </section>
                <section className="flex flex-col justify-center items-center gap-5 p-4">
                    <Title classNames={'text-5xl font-bold'} text={title} />
                    <div className={'flex flex-wrap gap-2 justify-center items-stretch'}>
                        {tags.split(' ').map((tag, index)=> (
                            <p key={index} className={'w-fit text-2xl border-2 border-[#06b6d4] text-white px-3 py-1 rounded'} style={{
                                textShadow: '0 0 7px #06b6d4, 0 0 10px #1e1b4b,0 0 21px #0c4a6e'
                            }}>{tag}</p>
                        ))}
                    </div>
                    <p className={'text-lg text-center my-5'}>
                        {removeHtmlTags(body_html)}
                    </p>
                    <span className={'w-fit text-2xl border-2 border-[#06b6d4] text-white px-3 py-1 rounded'} style={{
                        textShadow: '0 0 7px #06b6d4, 0 0 10px #1e1b4b,0 0 21px #0c4a6e'
                    }}>
                      Bs {product.variants[0].price} BOB
                    </span>
                    <ProductViewItemsOrder maxQuantity={quantity}/>
                </section>
            </BgSvg>
        </main>
    )
}