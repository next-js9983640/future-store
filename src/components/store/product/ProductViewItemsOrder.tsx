"use client";
import { SyntheticEvent, useState } from "react";
import { FaCartShopping } from 'react-icons/fa6';
import styles from "./ProductViewItemsOrder.module.sass";


interface ProductViewItemsOrderProps {
    maxQuantity: number,
}

export const ProductViewItemsOrder = ({ maxQuantity }: ProductViewItemsOrderProps) => {
    const [counter, setCounter] = useState(1);

    const handleSubmit = (event: SyntheticEvent) => {
        event.preventDefault();
    };

    const handleSubtract = (event: SyntheticEvent) => {
        event.preventDefault();
        if (counter === 1) return;
        setCounter(counter - 1);
    }

    const handleAdd = (event: SyntheticEvent) => {
        event.preventDefault();
        if (counter === maxQuantity) return;
        setCounter(counter + 1);
    }

    return (
        <div className={'flex md:flex-row flex-col gap-5 '}>
            <div className={`flex flex-nowrap border border-[#06b6d4] justify-evenly items-center gap-5 px-3 py-1 rounded text-[1.5rem]`}
                 style={{
                     boxShadow: '0 0 10px #06b6d4 inset, 0 0 10px #06b6d4, 0 0 21px #06b6d4 inset, 0 0 21px #06b6d4, 0 0 31px #06b6d4 inset, 0 0 31px #06b6d4',
                     background: 'rgba(0, 0, 0, 0.5)',
                 }}
            >
                <button onClick={handleSubtract}>-</button>
                <p>{counter}</p>
                <button onClick={handleAdd}>+</button>
            </div>
            <form
                onSubmit={handleSubmit}
                className="text-center px-3 py-1 group relative inline-block overflow-hidden rounded border-gray-100 text-lg font-bold bg-clip-text
                      text-transparent bg-gradient-to-r from-blue-600 to-pink-600"
                style={{
                    boxShadow: '0 0 10px #06b6d4 inset, 0 0 10px #06b6d4, 0 0 21px #06b6d4 inset, 0 0 21px #06b6d4, 0 0 31px #06b6d4 inset, 0 0 31px #06b6d4',
                    background: 'rgba(0, 0, 0, 0.5)',
                }}
            >
                <span
                    className="ease absolute left-0 top-0 h-0 w-0 border-t-2 border-indigo-900 transition-all duration-200 group-hover:w-full"></span>
                <span
                    className="ease absolute right-0 top-0 h-0 w-0 border-r-2 border-indigo-900 transition-all duration-200 group-hover:h-full"></span>
                <span
                    className="ease absolute bottom-0 right-0 h-0 w-0 border-b-2 border-indigo-900 transition-all duration-200 group-hover:w-full"></span>
                <span
                    className="ease absolute bottom-0 left-0 h-0 w-0 border-l-2 border-indigo-900 transition-all duration-200 group-hover:h-full"></span>
                <button
                    className={`text-white text-2xl flex justify-center items-center gap-2 px-3 py-1 rounded`}
                    type="submit"
                    style={{
                        textShadow: '0 0 7px #06b6d4, 0 0 10px #1e1b4b,0 0 21px #0c4a6e'
                    }}
                >
                    <FaCartShopping/>
                    <span className={'w-fit  px-3 py-1 rounded'}>Añadir al carrito</span>
                </button>
            </form>
            <button className={'absolute flex gap-2 top-0 left-0 p-4'} style={{
                boxShadow: '0 0 10px #06b6d4 inset, 0 0 10px #06b6d4, 0 0 21px #06b6d4 inset, 0 0 21px #06b6d4, 0 0 31px #06b6d4 inset, 0 0 31px #06b6d4',
                background: 'rgba(0, 0, 0, 0.5)',
            }} onClick={() => window.history.back()}>
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5}
                     stroke="currentColor" className="w-6 h-6">
                    <path strokeLinecap="round" strokeLinejoin="round" d="M6.75 15.75 3 12m0 0 3.75-3.75M3 12h18"/>
                </svg>

                volver atras
            </button>
        </div>
    )
};