import Image from "next/image";

export function CustomImage({public_id, format, BLUR_PLACEHOLDER, classNames}: {public_id: string, format: string, BLUR_PLACEHOLDER: string, classNames?: string}){
    return (
        <Image
            alt="Next.js Conf photo"
            className={`${classNames} transform rounded-lg brightness-90 transition will-change-auto group-hover:brightness-110`}
            style={{ transform: "translate3d(0, 0, 0)" }}
            placeholder="blur"
            blurDataURL={BLUR_PLACEHOLDER}
            src={`https://res.cloudinary.com/${process.env.NEXT_PUBLIC_CLOUDINARY_CLOUD_NAME}/image/upload/c_scale,w_720/${public_id}.${format}`}
            width={720}
            height={480}
            unoptimized
            sizes="(max-width: 640px) 100vw,
                  (max-width: 1280px) 50vw,
                  (max-width: 1536px) 33vw,
                  25vw"
        />
    )
}