"use client";
import { useSearchParams, usePathname } from 'next/navigation'
import {generatePagination} from "@/utils/utils";
import {PaginationArrow} from "@/components/ui/Gallery/PaginationArrow";
import {PaginationNumber} from "@/components/ui/Gallery/PaginationNumber";
export function Pagination({totalPages}:{totalPages: number}){
    const searchParams = useSearchParams()
    const pathName = usePathname()

    const currentPage = Number(searchParams.get('page')) || 1

    const createPageUrl = (pages: number)=>{
        const params = new URLSearchParams(searchParams)
        params.set('page', pages.toString())
        return`${pathName}?${params.toString()}`
    }
    return (
        <div className='inline-flex'>
            <PaginationArrow href={createPageUrl(currentPage-1)} direction="left" isDisabled={currentPage <= 1}/>
            <div className="flex h-10 w-auto items-center justify-center text-center bg-indigo-950 rounded-md py-5 px-3 text-xl font-bold">
                {currentPage} de {totalPages}
            </div>
            <PaginationArrow
                direction="right"
                href={createPageUrl(currentPage + 1)}
                isDisabled={currentPage >= totalPages}
            />
        </div>
    )
}