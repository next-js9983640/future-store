export function Input({ type, label, placeholder, name, children }:{ type: string, label:string, placeholder: string, name: string, children: React.ReactNode }) {
    return (
        <div className="dark:text-white text-gray-900">
            <label htmlFor={name}
                   className="block text-sm font-medium leading-6 capitalize">
                {label}
            </label>
            <div className="mt-2">
                <input
                    id={name}
                    name={name}
                    type={type}
                    autoComplete={name}
                    placeholder={placeholder}
                    className="block w-full rounded-md border-0 py-1.5 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6 dark:bg-border-color dark:placeholder-gray-400 dark:ring-gray-600 dark:focus:ring-indigo-600 dark:ring-opacity-50 dark:ring-offset-gray-900 dark:ring-offset-opacity-50"
                />
            </div>
            {children}
        </div>
    )
}