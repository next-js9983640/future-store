import Marquee from "react-fast-marquee";

interface MarqueeProps {
    className?: string
    children?: React.ReactNode
    variant?: 'primary' | 'secondary'
}
export function MarqueeProvider({className = '', children, variant = 'primary',}:MarqueeProps){

    const rootClassName = `marquee ${className}`
    return(
        <Marquee className={rootClassName} delay={1} speed={20} pauseOnHover={true} autoFill={false} >
            {children}
        </Marquee>
    )
}