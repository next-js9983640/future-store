import styles from './Title.module.sass'
export function Title({text, classNames, styles}:{text: string, classNames?: string, styles?: any}){
    return (
        <h1 className={`bg-clip-text text-transparent bg-gradient-to-r from-blue-600 to-pink-600 ${classNames} font-bold text-center `}
            style={styles}
        >
            {text}
        </h1>
    )
}