export const env = {
    shopifyHostName: process.env.SHOPIFY_HOSTNAME as string,
    shopifyApiKey: process.env.SHOPIFY_API_KEY as string,
    openAirApiKey: process.env.OPENAI_API_KEY as string,
    supabaseUrl: process.env.NEXT_PUBLIC_SUPABASE_URL as string,
    supabaseAnonKey: process.env.NEXT_PUBLIC_SUPABASE_ANON_KEY as string,
    shopifyGraphqlEndpoint: process.env.SHOPIFY_GRAPHQL_ENDPOINT as string,
    shopifyStoreFrontToken: process.env.SHOPIFY_STOREFRONT_TOKEN as string,
}