import {urls} from "@/services/api/urls";

export const getDataConfig = {
    shopifyGetProductsHeaders: urls.shopify.products.all,
    shopifyGetMainProductsHeaders: urls.shopify.products.MainProducts,
    shopifyGetCollectionsHeaders: urls.shopify.collections.all,
    shopifyGetProductsByCollection: (id: string)=>urls.shopify.collections.products(id),
    shopifyGetCustomersHeaders: urls.shopify.customers.all
}