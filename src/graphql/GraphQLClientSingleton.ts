// @ts-ignore
import { GraphQLClient } from "graphql-request";
import { env } from '@/config/env';

export class GraphQLClientSingleton {
    private readonly endpoint: string = env.shopifyGraphqlEndpoint;

    private static instance: GraphQLClientSingleton;

    static getInstance(): GraphQLClientSingleton {
        if (!this.instance) {
            this.instance = new GraphQLClientSingleton();
        }
        return this.instance;
    }

    getClient(): GraphQLClient {
        return new GraphQLClient(this.endpoint, {
            headers: {
                'Shopify-Storefront-Private-Token': env.shopifyStoreFrontToken,
            },
        });
    }
}