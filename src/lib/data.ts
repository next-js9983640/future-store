import {NavigationType} from "@/lib/definitions";

export const Navigation: NavigationType[] = [
    {
        name: "Home",
        href: "/"
    },
    {
        name: "Store",
        href: "/store"
    },
    {
        name: "Gallery",
        href: "/gallery"
    },
    {
        name: "Chat",
        href: "/chat"
    },
    {
        name: "Iniciar sesión",
        href: "/signin"
    },
    {
        name: "Customers",
        href: "/customers"
    }
]