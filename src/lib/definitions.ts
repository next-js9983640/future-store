export interface NavigationType {
    name: string,
    href: string
}

interface Image{
    id: any;
    src: string;
    height: number;
    width: number;
}
export interface ImageType extends Image{
    path: string;
    public_id: string;
    secure_url: string;
    original_filename: string;
    blurImage?: string;
}


export interface ShopifyProductType {
    body_html:string,
    created_at: string,
    vendor: string,
    handle:string,
    id: string,
    image: Image,
    status: "active" | "draft",
    tags: string,
    title: string,
    price: string,
    variants: {
        price: string
    }[],
    quantity: number;
}

export interface Config {
    url: string,
    cache?: RequestCache,
    method: string,
    headers?: Headers,
    key: string
}

export interface ShopifyCollectionsType {
    id: number,
    handle: string,
    title: string,
}

export interface ClientType{
    customerCreate: {
        customer: {
            firstName: string,
            lastName: string,
            email: string,
            phone: string
        },
        customerUserErrors: customerUserErrorsType[]
    }
}
export interface customerUserErrorsType{
    field: string[],
    message: string,
    code: string
}

export type State = {
    errors?:{
        firstName?: string[],
        lastName?: string[],
        email?: string[],
        phone?: string[],
        password?: string[],
        rePassword?: string[],
    };
    message?: string | null;
}

export interface ErrorType{
    field: string[],
    message: string,
    code: string
}