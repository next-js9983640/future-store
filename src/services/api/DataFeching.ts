import {Config, ShopifyProductType} from "@/lib/definitions";

export async function dataFeching(config: Config, id?: string){
    const {url, cache, method, headers, key} = config
    try {
        const dataUrl = id ? `${url}?ids=${id}` : url
        const response = await fetch(dataUrl,  {
            cache: cache,
            method: method,
            headers: headers
        })
        const  res = await response.json()
        return res[key]

    }catch (e){
        throw new Error((e as Error).message)
    }
}