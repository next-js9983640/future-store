import {env} from "@/config/env";

export const urls = {
    shopify: {
        products:{
            all: {
                url: `${env.shopifyHostName}/admin/api/2023-10/products.json`,
                cache: 'no-cache' as RequestCache,
                method: 'GET',
                headers: new Headers({
                    "X-Shopify-Access-Token": env.shopifyApiKey,
                }),
                key: 'products'
            },
            MainProducts:{
                url: `${env.shopifyHostName}/admin/api/2023-10/collections/304076095643/products.json`,
                cache: 'no-cache' as RequestCache,
                method: 'GET',
                headers: new Headers({
                    "X-Shopify-Access-Token": env.shopifyApiKey,
                }),
                key: 'products'
            }
        },
        collections:{
            all: {
                url: `${env.shopifyHostName}/admin/api/2023-10/smart_collections.json`,
                cache: 'no-cache' as RequestCache,
                method: 'GET',
                headers: new Headers({
                    "X-Shopify-Access-Token": env.shopifyApiKey,
                }),
                key: 'smart_collections'
            },
            products: (id: string)=> {
                return{
                    url: `${env.shopifyHostName}/admin/api/2023-10/collections/${id}/products.json`,
                    cache: 'no-cache' as RequestCache,
                    method: 'GET',
                    headers: new Headers({
                        "X-Shopify-Access-Token": env.shopifyApiKey,
                    }),
                    key: 'products'
                }
            }
        },
        customers:{
            all:{
                url: `${env.shopifyHostName}/admin/api/2023-10/customers.json`,
                cache: 'no-cache' as RequestCache,
                method: 'GET',
                headers: new Headers({
                    "X-Shopify-Access-Token": env.shopifyApiKey,
                }),
                key: 'customers'
            }
        }
    }
}