import {GraphQLClientSingleton} from "@/graphql/GraphQLClientSingleton";
import {customerAccessTokenCreateMutation} from "@/graphql/mutations/customerAccessTokenCreate";
import {cookies} from "next/headers";
import bcrypt from 'bcrypt'

interface CreateCustomerAccessTokenType{
    customerAccessTokenCreate: {
        customerAccessToken: {
            accessToken: string,
            expiresAt: string
        },
        customerUserErrors: {
            message: string
        }[]
    }
}

export async function createAccessToken(email: string, password: string){
    const cookiesStore = cookies()
    const client = GraphQLClientSingleton.getInstance().getClient()
    const { customerAccessTokenCreate }: CreateCustomerAccessTokenType = await client.request(customerAccessTokenCreateMutation, {
        "email": email,
        "password": password
    })
    const {customerAccessToken, customerUserErrors} = customerAccessTokenCreate

    if(customerUserErrors.length > 0 && !customerAccessToken){
        const errorMessage = customerUserErrors[0].message; // Suponiendo que el mensaje está en la propiedad 'message'
        return new Error(errorMessage)
    }
    const {accessToken, expiresAt}  = customerAccessToken
    const salt = bcrypt.genSaltSync(10);

    if(accessToken){
        const hash = bcrypt.hashSync(accessToken, salt);
        cookiesStore.set("accessToken", hash, {
            path: "/",
            expires: new Date(expiresAt),
            httpOnly: true,
            sameSite: "strict"
        })
    }
    return accessToken
}

// 4z@G9!KEe*j6*P
// 4z@G9!KEe*j6*P
// d8z*a@F%SVQ&ap