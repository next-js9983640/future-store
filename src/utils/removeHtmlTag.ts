export const removeHtmlTags = (htmlText: string) => {
    return htmlText.replace(/<\/?[^>]+(>|$)/g, '');
}